import Product from "./models/Product.model";
import Vendor from "./models/Vendor.model";

/**
    * suggested endpoint
    * GET /checkout
 */

const response = {
    // step #1 in UI 
    products: [
        {
            amount: Number,
            vendor: Vendor,
            item: Product
        }
    ],
    totalProuctsPrice: Number,
    tax: Number,
    shippingCost: Number,

    // step #2 in UI
    addresses: [
        // this will have data only if the user supplied an address before, if not then the user will have to create a new shipping address
        {
            addressName: String,
            name: {
                firstName: String,
                lastName: String,
            },
            phone: Number,
            preferredDeliveryTime: String, // enums ['09:00 to 13:00', '13:00 to 20:00']
            location: {
                address: String,
                coords: {
                    lat: Number,
                    lng: Number,
                },
            },
            isDefault: Boolean,
        }
    ],

    // step #3 in UI
    payments: {
        // if the used a credit card before it will show here, if not then he will need to choose a new payment method 
        creditCards: [
            {
                owner: String,
                number: Number,
                expDate: Date,
            }
        ],
        // this info is the same for all users (check the UI design in page called "Cart details – cards payment")
        bankTransactionDetails: {
            bankName: String,
            transactionNumber: String
        },
        stcPay: {
            number: Number
        }
    }
}


/**
    * suggested endpoint
    * POST /checkout
 */

 const request = {
     cart: ID, // cart ID
     shippingAddress: ID, // shipping address ID
     paymentMethod: ID, // payment method ID
 }
 const response = {
     success: Boolean,
     order: Order
 }