const User = {
    name: String,
    logo: Image,
    activity_type: String,
    name: {
        firstName: String,
        lastName: String,
    },
    phone: Number,
    email: String,
    region: String,
    city: String,
    delegate: {
        name: String,
        phone: Number,
    },
    password: String,
    type: String, // enums ['vendor', 'pos']
    allowNotifications: Boolean,
    
    // bellow fields are only for Vendors
    facilityIdentificationNumber: Number,
    taxIdentificationNumber: Number,
    cover: Image,
}

export default User;
