import Review from "./Review.model";
import Vendor from "./Vendor.model";

const Product = {
    name: String,
    image: Image,
    price: Number,
    ratings: Number, // e.g : 4.3 (out of 5)
    discount: Number,
    discountExprationDate: Date,
    profitPercentage: Number,

    // the below fields are required only in single product page 
    features: {
        minAmountAllowed: Number,
        itemWight: Number,
        singlePackageWight: Number,
        packagesCount: Number,
        singleBoxWight: Number,
    },
    productsDescription: String,
    vendor: Vendor,
    reviews: Review
}

export default Product;
