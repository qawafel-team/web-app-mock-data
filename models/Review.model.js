import User from "./User.model";

const Review = {
    user: User,
    date: Date,
    rate: Number, // e.g : 4.3 (out of 5)
    text: String,
    images: [Image, Image]
}

export default Review;
