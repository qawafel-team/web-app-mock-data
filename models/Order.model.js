import Product from "./Product.model";

const Order = {
    orderNumber: Number,
    date: Date,
    paymentMethod: PaymentMethod,
    address: Address,
    status: String, // enums: ['proccessing', 'rejected', 'preparing', 'delivering', 'delivered']
    producrs: [Product, Product, Product]
}

export default Order;