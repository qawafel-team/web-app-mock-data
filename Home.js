import Vendor from './models/Vendor.model';
import Category from './models/Category.model';
import Product from './models/Product.model';

/**
    * suggested endpoint
    * GET /home
 */
const response = {
    mainSlider: [
        {
            image: Image,
            text: String,
            link: String, // optional
        }
    ],
    offersSection: {
        vendor: Vendor,
        saleExpirationDate: Date,
        productOnSale: [Product, Product, Product]
    },
    topSelling: [Product, Product, Product],
    topCategories: [Category, Category, Category],
    topVendor: [Vendor, Vendor, Vendor]
}
