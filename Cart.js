import Product from "./models/Product.model";
import Vendor from "./models/Vendor.model";

/**
    * suggested endpoint
    * GET /cart
 */

const response = {
    products: [
        {
            amount: Number,
            vendor: Vendor,
            item: Product
        }
    ],
    totalProuctsPrice: Number,
    tax: Number,
    shippingCost: Number, // N/A because its in checkout
}

/**
    * Add product to cart
    * suggested endpoint
    * POST /cart
 */

const request = {
    productId: ID,
    amount: Number
}


/**
 * TESTING 
 * POST {{BASE_URL}}/SraCartContent
 * 
 * 1- a user can add a product to other user cart ( this should be not allowed )
 * 
 */

/**
 * TESTING 
 * GET {{BASE_URL}}/SraCartContent?user_id={{USER_ID}}
 * 
 * 1- a user can view other user cart ( this should be not allowed )
 * 
 */