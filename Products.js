import Product from "./models/Product.model";
import Review from "./models/Review.model";

/**
    * suggested endpoint
    * GET /products/:productID
 */
const response = {
    product: Product,
    reviews: Review,
}

/**
 * TESTING 
 * GET {{BASE_URL}}/products/454
 * 
 * 1- we need to extend the company/vendor (id, name, logo, description)
 * 2- we need to extend the product reviews 
 * 
 */