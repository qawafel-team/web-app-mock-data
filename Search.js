import Product from "./models/Product.model";

/**
    * suggested endpoint
    * POST /search
 */

const request = {
    text: String,
    price: {
        from: Number,
        to: Number,
    },
}

const response = {
    results: [Product, Product, Product],
    keywordsSuggestions: [String, String, String] // if there is non results found then we suggest some similiar search terms to the user
}