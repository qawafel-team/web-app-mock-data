import Product from './models/Product.model';
import User from './models/User.model';
import Order from './models/Order.model';

/**
    * suggested endpoint
    * GET /account
 */
const response = {
    user: User,
    orders: [Order, Order, Order],
    shippingAddresses: [Address, Address, Address],
    wallet: {
        currentBalane: Number,
        paymentMethods: [CreditCard, CreditCard, CreditCard],
    },
    whishlist: [Product, Product, Product]
}