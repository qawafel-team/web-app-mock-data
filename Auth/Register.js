import User from '../models/User.model'

/**
    * suggested endpoint
    * POST /register
 */
const request = {
    logo: Image,
    name: String,
    activity_type: String,
    name: {
        firstName: String,
        lastName: String,
    },
    phone: Number,
    email: String,
    region: String,
    city: String,
    delegate: {
        name: String,
        phone: Number,
    },
    password: String,
    type: String, // enums ['vendor', 'pos']
    
    // bellow fields are only for Vendors
    facilityIdentificationNumber: Number,
    taxIdentificationNumber: Number,
}
const response = {
    user: User,
    accessToken: String, // JWT,
}