import User from '../models/User.model'

/**
    * suggested endpoint
    * POST /login
 */
const request = {
    email: String,
    password: String
}
const response = {
    user: User,
    accessToken: String, // JWT,
}


/**
    * Forgot password
    * suggested endpoint
    * POST /forgot-password
 */
const request = {
    email: String
}
const response = {
    success: Boolean, // whether the reset email was sent to the user or not
}